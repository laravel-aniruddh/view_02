<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('home',function(){
    return view('home');
});

Route::get('about',function(){
    return view('about.aboutme');
});

Route::view('contact','contactme');
Route::view('profile','profiles.mypage');


// Creating & Rendering Views
// pass data in view file

Route::get('name',function(){
    return view('name',['name'=>'007']);
});

Route::get('jb',function(){
    return view('jb',['jb1'=>'007','jb2'=>'oo7']);
});


// pass data in view file with method

Route::get('oo7',function(){
    return view('hollywood.movies')->with('jb','007');
});